# container-s3cmd

A container image for the [s3cmd CLI tool](https://github.com/s3tools/s3cmd)

# Configuration

The container uses `/etc/s3cfg` as the default configuration file, if you have
an existing configuration you can pass it like this:

```
podman run --rm -it --volume /path/to/s3cfg:/etc/s3cfg thekad/s3cmd:latest
```

If you don't have one, you can create one using the same container like so:

```
podman run --rm -it thekad/s3cmd:latest --ssl --access_key=XXXXXXX --secret_key=XXXXXX --dump-config
```

You can adjust any settings then
